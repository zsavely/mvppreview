package com.szagurskii.mvp.main;

import com.szagurskii.mvp.Presenter;

import java.util.UUID;

public class MainPresenter extends Presenter<MainView> {
  public void generateString() {
    view().setText(UUID.randomUUID().toString());
  }

  public void finishi() {
    view().finish();
  }
}
