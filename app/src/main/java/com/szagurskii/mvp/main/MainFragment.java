package com.szagurskii.mvp.main;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.szagurskii.mvp.R;

public class MainFragment extends Fragment implements MainView {

  private TextView tv;

  final MainPresenter mainPresenter = new MainPresenter();

  @Nullable @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_main, container, false);

    tv = (TextView) view.findViewById(R.id.tv);

    Button action0View = (Button) view.findViewById(R.id.btn_action0);
    Button finishView = (Button) view.findViewById(R.id.btn_finish);

    action0View.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mainPresenter.generateString();
      }
    });
    finishView.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mainPresenter.generateString();
      }
    });

    return view;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mainPresenter.bindView(this);
  }

  @Override public void onDestroyView() {
    mainPresenter.unbindView(this);
    super.onDestroyView();
  }

  @Override public void setText(@NonNull String value) {
    tv.setText(value);
  }

  @Override public void finish() {
    getActivity().finish();
  }
}
