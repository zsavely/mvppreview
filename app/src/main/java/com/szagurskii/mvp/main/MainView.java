package com.szagurskii.mvp.main;

import android.support.annotation.NonNull;

public interface MainView {
  void setText(@NonNull String value);

  void finish();
}
