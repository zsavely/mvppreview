package com.szagurskii.mvp;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

public abstract class Presenter<V> {
  private static final String TAG = "Presenter";

  @Nullable private volatile V view;

  @CallSuper public void bindView(@NonNull V view) {
    final V previousView = this.view;

    if (previousView != null) {
      Log.w(TAG, "bindView", new IllegalStateException("Previous view is not unbounded! previousView = " + previousView));
    }

    this.view = view;
  }

  @CallSuper public void unbindView(@NonNull V view) {
    final V previousView = this.view;

    this.view = null;

    if (previousView != view) {
      Log.w(TAG, "unbindView", new IllegalStateException("Unexpected view! previousView = " + previousView + ", view to unbind = " + view));
    }
  }

  @Nullable protected V view() {
    return view;
  }
}
